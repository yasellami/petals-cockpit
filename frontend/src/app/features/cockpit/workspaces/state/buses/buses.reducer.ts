/**
 * Copyright (C) 2017-2019 Linagora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
  JsTable,
  mergeOnly,
  putAll,
  removeById,
  updateById,
} from '@shared/helpers/jstable.helper';
import { fold, toggleFold, unfold } from '@shared/helpers/reducers.helper';
import {
  IBusBackendDetails,
  IBusBackendSSE,
} from '@shared/services/buses.service';
import { Buses } from '@wks/state/buses/buses.actions';
import { Workspaces } from '@wks/state/workspaces/workspaces.actions';
import {
  busesTableFactory,
  busRowFactory,
  IBusesTable,
} from './buses.interface';

export namespace BusesReducer {
  type All =
    | Buses.Fetched
    | Buses.Added
    | Buses.SetCurrent
    | Buses.FetchDetails
    | Buses.FetchDetailsError
    | Buses.FetchDetailsSuccess
    | Buses.Fold
    | Buses.Unfold
    | Buses.ToggleFold
    | Buses.CancelSelect
    | Buses.ToggleSelect
    | Buses.Detach
    | Buses.DetachSuccess
    | Buses.DetachError
    | Buses.Detached
    | Workspaces.Clean;

  export function reducer(
    table = busesTableFactory(),
    action: All
  ): IBusesTable {
    switch (action.type) {
      case Buses.FetchedType: {
        return fetched(table, action.payload);
      }
      case Buses.AddedType: {
        return added(table, action.payload);
      }
      case Buses.SetCurrentType: {
        return setCurrent(table, action.payload);
      }
      case Buses.FetchDetailsType: {
        return fetchDetails(table, action.payload);
      }
      case Buses.FetchDetailsErrorType: {
        return fetchDetailsError(table, action.payload);
      }
      case Buses.FetchDetailsSuccessType: {
        return fetchDetailsSuccess(table, action.payload);
      }
      case Buses.FoldType: {
        return fold(table, action.payload);
      }
      case Buses.UnfoldType: {
        return unfold(table, action.payload);
      }
      case Buses.ToggleFoldType: {
        return toggleFold(table, action.payload);
      }
      case Buses.CancelSelectType: {
        return cancelSelect(table, action.payload);
      }
      case Buses.ToggleSelectType: {
        return toggleSelect(table, action.payload);
      }
      case Buses.DetachType: {
        return detach(table, action.payload);
      }
      case Buses.DetachErrorType: {
        return detachError(table, action.payload);
      }
      case Buses.DetachSuccessType: {
        return detachSuccess(table, action.payload);
      }
      case Buses.DetachedType: {
        return detached(table, action.payload);
      }
      case Workspaces.CleanType: {
        return busesTableFactory();
      }
      default:
        return table;
    }
  }

  function fetched(table: IBusesTable, payload: JsTable<IBusBackendSSE>) {
    return mergeOnly(table, payload, busRowFactory);
  }

  function added(table: IBusesTable, payload: JsTable<IBusBackendSSE>) {
    return putAll(table, payload, busRowFactory);
  }

  function setCurrent(
    table: IBusesTable,
    payload: { id: string }
  ): IBusesTable {
    return {
      ...table,
      selectedBusId: payload.id,
    };
  }

  function fetchDetails(table: IBusesTable, payload: { id: string }) {
    return updateById(table, payload.id, { isFetchingDetails: true });
  }

  function fetchDetailsSuccess(
    table: IBusesTable,
    payload: { id: string; data: IBusBackendDetails }
  ) {
    return updateById(table, payload.id, {
      ...payload.data,
      isFetchingDetails: false,
    });
  }

  function fetchDetailsError(table: IBusesTable, payload: { id: string }) {
    return updateById(table, payload.id, { isFetchingDetails: false });
  }

  function cancelSelect(table: IBusesTable, payload: { id: string }) {
    return updateById(table, payload.id, {
      isBusSelectedForDetachment: false,
    });
  }

  function toggleSelect(table: IBusesTable, payload: { id: string }) {
    return updateById(table, payload.id, {
      isBusSelectedForDetachment: !table.byId[payload.id]
        .isBusSelectedForDetachment,
    });
  }

  function detachSuccess(
    table: IBusesTable,
    payload: { id: string }
  ): IBusesTable {
    return updateById(table, payload.id, { isDetaching: true });
  }

  function detached(table: IBusesTable, payload: { id: string }) {
    return removeById(table, payload.id);
  }

  function detach(table: IBusesTable, payload: { id: string }) {
    return updateById(table, payload.id, { isDetaching: true });
  }

  function detachError(table: IBusesTable, payload: { id: string }) {
    return updateById(table, payload.id, { isDetaching: false });
  }
}
